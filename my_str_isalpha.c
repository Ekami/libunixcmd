/*
** my_str_isalpha.c for my_str_isalpha in /home/godard_b//workspace/progs/Jour_06
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Tue Oct  9 16:18:54 2012 tuatini godard
** Last update Tue Oct  9 16:25:21 2012 tuatini godard
*/

int	my_str_isalpha(char *str)
{
  int	i;

  i = 0;
  while (str[i] != 0)
    {
      if (!((str[i] >= 65 && str[i] <= 90) || (str[i] >= 97 && str[i] <= 122)))
	return (0);
      i = i + 1;
    }
  return (1);
}
