/*
** my_power_rec.c for my_power_rec.c in /home/godard_b//test/Jour_05
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Fri Oct  5 22:40:11 2012 tuatini godard
** Last update Mon Oct  8 09:36:50 2012 tuatini godard
*/

int	my_power_rec(int nb, int power)
{
  if (power < 0)
    return (0);
  else if (power == 0)
    return (1);
  else if (power == 1)
    return (nb);
  else
    return (nb * my_power_rec(nb, power - 1));
}
