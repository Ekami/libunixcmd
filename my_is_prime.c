/*
** my_is_prime.c for my_is_prime in /home/e-kami/workspace/epitech/Jour_05
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Wed Oct 17 20:39:39 2012 tuatini godard
** Last update Thu Oct 18 22:26:38 2012 tuatini godard
*/

int	my_is_prime(int nb)
{
  int	i;
  int	nb_diviser;
  float	tempF;
  int	tempI;

  i = 1;
  nb_diviser = 0;
  tempF = 0;
  tempI = 0;
  while (i <= nb)
    {
      tempF = (float) nb / i;
      tempI = nb / i;
      if (tempF == (float) tempI)
	nb_diviser = nb_diviser + 1;
      if (nb_diviser > 2)
	return (0);
      i = i + 1;
    }
  if (nb_diviser == 2)
    return (1);
  return (0);
}
