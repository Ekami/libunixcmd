/*
** my_strcmp.c for my_strcmp.c in /home/godard_b//workspace/progs/Jour_06
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Mon Oct  8 17:39:36 2012 tuatini godard
** Last update Tue Oct  9 09:56:55 2012 tuatini godard
*/

int	my_strcmp(char *s1, char *s2)
{
  int	i;
  int	total_count;
  int	s1_size;
  int	s2_size;
  int	max;

  i = 0;
  s1_size = my_strlen(s1);
  s2_size = my_strlen(s2);
  total_count = 0;
  if (s1_size > s2_size)
    max = s1_size;
  else
    max = s2_size;
  while (i < max)
    {
      total_count = total_count + (s1[i] - s2[i]);
      if (s1[i] != s2[i])
	return (total_count);
      i = i + 1;
    }
  return (total_count);
}
