/*
** print_byte_in_hex.c for print_byte_int_hex in /home/e-kami/workspace/epitech/libmy
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Fri Oct 19 17:14:38 2012 tuatini godard
** Last update Sun Nov 18 18:40:57 2012 tuatini godard
*/

void	reverse_print(char tab[], int tab_size)
{
  int	i;

  i = tab_size - 1;
  while (tab[i] == '0' && i >= 0)
    i--;
  while (i >= 0)
    {
      my_putchar(tab[i]);
      i = i - 1;
    }
}

void	print_int_in_hex(unsigned long long nb, int byte_size, int up)
{
  int	i;
  int	tmp;
  int	mask;
  char	tab[sizeof(char) * (byte_size * 2) + 1];

  i = 0;
  mask = 15;
  while (i < byte_size * 2)
    {
      tmp = mask & nb;
      if (tmp <= 9)
	tab[i] = tmp + 48;
      else
	{
	  if (up)
	    tab[i] = tmp + 55;
	  else
	    tab[i] = tmp + 87;
	}
      nb = nb >> 4;
      i = i + 1;
    }
  tab[i] = 0;
  reverse_print(tab, byte_size * 2);
}

void	print_ptr_in_hex(void *ptr)
{
  my_putstr("0x");
  print_int_in_hex((unsigned long long) ptr, sizeof(void *), 0);
}

void	print_int_in_oct(unsigned long long nb, int byte_size)
{
  int	i;
  int	tmp;
  int	mask;
  char	tab[sizeof(char) * (byte_size * 4) + 1];

  i = 0;
  mask = 7;
  while (i < byte_size * 4)
    {
      tmp = mask & nb;
      tab[i] = tmp + 48;
      nb = nb >> 3;
      i = i + 1;
    }
  tab[i] = 0;
  reverse_print(tab, i);
}

void	print_int_in_bits(unsigned long long nb, int byte_size)
{
  int	i;
  int	tmp;
  int	mask;
  char	tab[sizeof(char) * (byte_size * 8) + 1];

  i = 0;
  mask = 1;
  while (i < byte_size * 8)
    {
      tmp = mask & nb;
      tab[i] = tmp + 48;
      nb = nb >> 1;
      i = i + 1;
    }
  tab[i] = 0;
  reverse_print(tab, i);
}
