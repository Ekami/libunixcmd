/*
** my_find_prime_sup.c for my_find_prime_sup in /home/e-kami/workspace/epitech/Jour_05
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Thu Oct 18 22:05:14 2012 tuatini godard
** Last update Sat Oct 20 17:47:03 2012 tuatini godard
*/

int	my_find_prime_sup(int nb)
{
  if (!my_is_prime(nb))
    my_find_prime_sup(nb + 1);
  else
    return (nb);
}
