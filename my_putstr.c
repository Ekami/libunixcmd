/*
** my_putstr.c for my_putstr.c in /home/godard_b//test/Jour_04
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Thu Oct  4 18:47:11 2012 tuatini godard
** Last update Thu Oct  4 18:58:25 2012 tuatini godard
*/

int	my_putstr(char *str)
{
  int	i;

  i = 0;
  while (*(str + i) != '\0')
    {
      my_putchar(*(str + i));
      i = i + 1;
    }
}
