/*
** my_str_isupper.c for my_str_islupper in /home/godard_b//workspace/progs/Jour_06
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Tue Oct  9 17:21:55 2012 tuatini godard
** Last update Tue Oct  9 17:31:46 2012 tuatini godard
*/

int	my_str_isupper(char *str)
{
  int	i;

  i = 0;
  while (str[i] != 0)
    {
      if (!(str[i] >= 65 && str[i] <= 90))
	  return (0);
      i = i + 1;
    }
  return (1);
}
