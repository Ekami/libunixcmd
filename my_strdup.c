/*
** my_strdup.c for my_strdup in /home/godard_b//Temp/jr8/ex_01
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Thu Oct 11 11:49:36 2012 tuatini godard
** Last update Thu Oct 11 14:31:47 2012 tuatini godard
*/

#include <stdlib.h>

char	*my_strdup(char *src)
{
  int	nbr;
  int	i;
  char	*p;

  i = 0;
  nbr = my_strlen(src);
  p = malloc(nbr);
  while (src[i])
    {
      p[i] = src[i];
      i = i + 1;
    }
  return (p);
}
