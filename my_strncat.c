/*
** my_strncat.c for my_strncat in /home/godard_b//workspace/progs/Jour_07
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Wed Oct 10 12:07:18 2012 tuatini godard
** Last update Wed Oct 10 16:03:13 2012 tuatini godard
*/

char	*my_strncat(char *dest, char *src, int nb)
{
  int	i;
  char	*p;
  int	src_counter;

  i = 0;
  p = &dest[0];
  src_counter = my_strlen(dest);
  while (src[i] != 0 && i < nb)
    {
      p[src_counter + i] = src[i];
      i = i + 1;
    }
  p[src_counter + i] = 0;
  return (dest);
}
