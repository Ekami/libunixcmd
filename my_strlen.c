/*
** my_strlen.c for my_strlen.c in /home/godard_b//test/Jour_04
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Thu Oct  4 19:09:40 2012 tuatini godard
** Last update Thu Oct  4 19:26:12 2012 tuatini godard
*/

int	my_strlen(char *str)
{
  int	i;

  i = 0;
  while (*(str + i) != '\0')
    {
      i = i + 1;
    }
  return (i);
}
