/*
** my_swap.c for my_swap.c in /home/godard_b//test/Jour_04
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Thu Oct  4 18:43:29 2012 tuatini godard
** Last update Thu Oct  4 19:33:25 2012 tuatini godard
*/

int	my_swap(int *a, int *b)
{
  int	c;

  c = *a;
  *a = *b;
  *b = c;
  return (0);
}
