/*
** my_square_root.c for my_square_root in /home/e-kami/workspace/epitech/Jour_05
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Wed Oct 17 20:08:41 2012 tuatini godard
** Last update Wed Oct 17 20:33:18 2012 tuatini godard
*/

int	my_square_root(int nb)
{
  int	i;
  int	temp;

  i = 0;
  temp = 0;
  while (nb > temp)
    {
      i = i + 1;
      temp = i * i;
    }
  if (temp == nb)
    return (i);
  else
    return (0);
}
