/*
** test.c for testC.c in /home/godard_b//test/C_progs
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Wed Oct  3 09:46:03 2012 tuatini godard
** Last update Sun Nov 18 02:49:40 2012 tuatini godard
*/

int	my_putchar(unsigned char c)
{
  write(1, &c, 1);
}
