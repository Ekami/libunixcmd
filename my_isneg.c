/*
** my_isneg.c for my_isneg.c in /home/godard_b//test/C_progs
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Wed Oct  3 14:55:27 2012 tuatini godard
** Last update Wed Oct 10 14:27:54 2012 tuatini godard
*/

int	my_isneg(int n)
{
  if (n < 0)
    {
      my_putchar('N');
    }
  else
    {
      my_putchar('P');
    }
  return (0);
}
