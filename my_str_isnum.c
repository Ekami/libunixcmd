/*
** my_str_isnum.c for my_str_isnum in /home/godard_b//workspace/progs/Jour_06
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Tue Oct  9 16:29:43 2012 tuatini godard
** Last update Tue Oct  9 17:17:26 2012 tuatini godard
*/

int	my_str_isnum(char *str)
{
  int	i;

  i = 0;
  while (str[i] != 0)
    {
      if (!(str[i] >= 48 && str[i] <= 57))
	return (0);
      i = i + 1;
    }
  return (1);
}
