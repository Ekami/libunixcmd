/*
** print_unsigned_decimal.c for print_unsigned_decimal in /home/godard_b/workspace/Projets/En_cours/my_printf/rendu/printf
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Sun Nov 18 02:17:33 2012 tuatini godard
** Last update Sun Nov 18 20:31:26 2012 tuatini godard
*/

#include <stdarg.h>
#include "my_printf.h"

void	hex_print(char *modifier, va_list ap, int lenght, int up)
{
  int	tmp;

  if (modifier[lenght - 2] == 'h' && modifier[lenght - 3] == 'h')
    {
      tmp = get_only_given_bits(va_arg(ap, unsigned int), sizeof(char));
      print_int_in_hex(tmp, sizeof(char), up);
    }
  else if (modifier[lenght - 2] == 'h')
    {
      tmp = get_only_given_bits(va_arg(ap, unsigned int), sizeof(short));
      print_int_in_hex(tmp, sizeof(short), up);
    }
  else if (modifier[lenght - 2] == 'l' && modifier[lenght - 3] == 'l')
    {
      tmp = sizeof(unsigned long long);
      print_int_in_hex(va_arg(ap, unsigned long long), tmp, up);
    }
  else if (modifier[lenght - 2] == 'l')
    print_int_in_hex(va_arg(ap, unsigned long), sizeof(unsigned long), up);
  else
    print_int_in_hex(va_arg(ap, unsigned int), sizeof(unsigned int), up);
}

void	oct_print(char *modifier, va_list ap, int lenght)
{
  int	tmp;

  if (modifier[lenght - 2] == 'h' && modifier[lenght - 3] == 'h')
    {
      tmp = get_only_given_bits(va_arg(ap, unsigned int), sizeof(char));
      print_int_in_oct(tmp, sizeof(char));
    }
  else if (modifier[lenght - 2] == 'h')
    {
      tmp = get_only_given_bits(va_arg(ap, unsigned int), sizeof(short));
      print_int_in_oct(tmp, sizeof(short));
    }
  else if (modifier[lenght - 2] == 'l' && modifier[lenght - 3] == 'l')
    {
      tmp = sizeof(unsigned long long);
      print_int_in_oct(va_arg(ap, unsigned long long), tmp);
    }
  else if (modifier[lenght - 2] == 'l')
    print_int_in_oct(va_arg(ap, unsigned long), sizeof(unsigned long));
  else
    print_int_in_oct(va_arg(ap, unsigned int), sizeof(unsigned int));
}

void		unsigned_dec_print(char *modifier, va_list ap, int lenght)
{
  unsigned int	tmp;

  if (modifier[lenght - 2] == 'h' && modifier[lenght - 3] == 'h')
    {
      tmp = sizeof(char);
      my_put_unsigned_nbr(get_only_given_bits(va_arg(ap, unsigned int), tmp));
    }
  else if (modifier[lenght - 2] == 'h')
    {
      tmp = va_arg(ap, unsigned int);
      my_put_unsigned_nbr(get_only_given_bits(tmp, sizeof(unsigned short)));
    }
  else if (modifier[lenght - 2] == 'l' && modifier[lenght - 3] == 'l')
    my_put_ulong_nbr(va_arg(ap, unsigned long long));
  else if (modifier[lenght - 2] == 'l')
    my_put_ulong_nbr(va_arg(ap, unsigned long));
  else
    my_put_unsigned_nbr(va_arg(ap, unsigned int));
}

void	bin_print(char *modifier, va_list ap, int lenght)
{
  int	tmp;

  if (modifier[lenght - 2] == 'h' && modifier[lenght - 3] == 'h')
    {
      tmp = get_only_given_bits(va_arg(ap, unsigned int), sizeof(char));
      print_int_in_bits(tmp, sizeof(char));
    }
  else if (modifier[lenght - 2] == 'h')
    {
      tmp = get_only_given_bits(va_arg(ap, unsigned int), sizeof(short));
      print_int_in_bits(tmp, sizeof(short));
    }
  else if (modifier[lenght - 2] == 'l' && modifier[lenght - 3] == 'l')
    {
      tmp = sizeof(unsigned long long);
      print_int_in_bits(va_arg(ap, unsigned long long), tmp);
    }
  else if (modifier[lenght - 2] == 'l')
    print_int_in_bits(va_arg(ap, unsigned long), sizeof(unsigned long));
  else
    print_int_in_bits(va_arg(ap, unsigned int), sizeof(unsigned int));
}

int	print_unsigned_decimal(char *modifier, va_list ap)
{
  int	lenght;

  lenght = my_strlen(modifier);
  if (modifier[lenght - 1] == 'x' || modifier[lenght - 1] == 'X')
    {
      hex_print(modifier, ap, lenght, modifier[lenght - 1] == 'x' ? 0 : 1);
      return (1);
    }
  else if (modifier[lenght - 1] == 'u')
    {
      unsigned_dec_print(modifier, ap, lenght);
      return (1);
    }
  else if (modifier[lenght - 1] == 'o')
    {
      oct_print(modifier, ap, lenght);
      return (1);
    }
  else if (modifier[lenght - 1] == 'b')
    {
      bin_print(modifier, ap, lenght);
      return (1);
    }
  return (0);
}
