/*
** my_strcat.c for my_strcat in /home/godard_b/workspace/libmy
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Tue Nov 27 21:25:23 2012 tuatini godard
** Last update Tue Nov 27 21:28:27 2012 tuatini godard
*/

char	*my_strcat(char *dest, char *src)
{
  int	i;
  int	count;

  i = 0;
  count = my_strlen(dest);
  while (src[i])
    {
      dest[count + i] = src[i];
      i = i + 1;
    }
  dest[count + i] = '\0';
  return (dest);
}
