/*
** bits_manager.c for bits_manager in /home/godard_b/workspace/Projets/En_cours/my_printf/rendu/printf
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Sun Nov 18 01:29:29 2012 tuatini godard
** Last update Sun Nov 18 16:17:40 2012 tuatini godard
*/

#include "my_printf.h"

int	my_putstr_oct(unsigned char *str)
{
  int	i;

  i = 0;
  while (str[i])
    {
      if (str[i] < 32 || str[i] >= 127)
	{
	  my_putchar('\\');
	  if (str[i] <= 7)
	    my_putstr("00");
	  else if (str[i] <= 63)
	    my_putchar('0');
	  print_int_in_oct(str[i], sizeof(char));
	}
      else
	my_putchar(str[i]);
      i = i + 1;
    }
  return (0);
}

int	get_only_given_bits(int nb, int bits_count)
{
  if (bits_count == 1)
    return (nb & 255);
  else if (bits_count == 2)
    return (nb & 65535);
}
