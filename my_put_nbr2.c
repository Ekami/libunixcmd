/*
** my_put_nbr.c for my_put_nbr in /home/e-kami/workspace/epitech/Jour_03
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Tue Oct 16 17:13:24 2012 tuatini godard
** Last update Sun Nov 18 18:13:58 2012 tuatini godard
*/

void	my_put_unsigned_nbr(unsigned int nb)
{
  if (nb < 0)
    {
      nb = -nb;
      my_putchar('-');
    }
  if (nb / 10 != 0)
    {
      my_put_unsigned_nbr(nb / 10);
      nb = nb % 10;
    }
  my_putchar(nb + 48);
}

void	my_put_long_nbr(long long nb)
{
  if (nb < 0)
    {
      nb = -nb;
      my_putchar('-');
    }
  if (nb / 10 != 0)
    {
      my_put_long_nbr(nb / 10);
      nb = nb % 10;
    }
  my_putchar(nb + 48);
}

void	my_put_ulong_nbr(unsigned long long nb)
{
  if (nb < 0)
    {
      nb = -nb;
      my_putchar('-');
    }
  if (nb / 10 != 0)
    {
      my_put_ulong_nbr(nb / 10);
      nb = nb % 10;
    }
  my_putchar(nb + 48);
}
