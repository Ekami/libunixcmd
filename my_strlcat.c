/*
** my_strncat.c for my_strncat in /home/godard_b//Temp/jr7
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Wed Oct 10 14:19:58 2012 tuatini godard
** Last update Mon Oct 22 09:33:34 2012 tuatini godard
*/

int	my_strlcat(char *dest, char *src, int nb)
{
  int	i;
  int	nbr;
  int	nbr2;
  char	*p;

  i = 0;
  p = &dest[0];
  nbr = my_strlen(dest);
  nbr2 = my_strlen(src);
  if (nbr + nbr2 < nb)
    {
      while ((src[i] != 0) && (i < nb))
	{
	  p[nbr + i] = src[i];
	  i = i + 1;
	}
      p[nbr + i] = '\0';
      return (nbr + nbr2);
    }
  return (nbr2);
}
