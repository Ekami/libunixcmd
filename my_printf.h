/*
** my_printf.h for my_printf in /home/e-kami/workspace/epitech/Projets/En_cours/printf/my_printf/rendu/printf
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Sat Nov 17 13:45:34 2012 tuatini godard
** Last update Sun Nov 18 01:20:40 2012 tuatini godard
*/

#ifndef MY_PRINTF_
#define MY_PRINTF_
#include <stdarg.h>

int	get_lenghtmodifier_size(const char *modif_start);
int	get_conversionspecifier_size(const char *modif_start, int ret);
int	get_only_given_bits(int nb, int bits_count);
int	print_signed_decimal(char *modifier, va_list ap);
int	print_unsigned_decimal(char *modifier, va_list ap);
void	print_ptr_in_hex(void *ptr);
void	print_int_in_bits(unsigned long long nb, int byte_size);
int	print_char(char *modifier, va_list ap);
int	print_string(char *modifier, va_list ap);
int	print_miscs(char *modifier, va_list ap);
int	print_int_in_hex(unsigned long long nb, int byte_size, int up);
void	print_int_in_oct(unsigned long long nb, int byte_size);
void	split_str(const char *str, char **buffer);
void	my_put_unsigned_nbr(unsigned int nb);
void	my_put_long_nbr(long long nb);
void	my_put_ulong_nbr(unsigned long long nb);
int	my_putstr_oct(unsigned char *);
void	free_all(char **str);

#endif
