/*
** my_showstr.c for my_showstr in /home/e-kami/workspace/epitech/Jour_06
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Fri Oct 19 00:00:18 2012 tuatini godard
** Last update Fri Oct 19 17:22:41 2012 tuatini godard
*/

int	my_showstr(char *str)
{
  int	i;

  i = 0;
  while (str[i] != 0)
    {
      if ((str[i] >= 0 && str[i] <= 31) || str[i] == 0x7F)
	{
	  my_putchar('\\');
	  print_byte_in_hex(str[i]);
	}
      else
	my_putchar(str[i]);
      i = i + 1;
    }
  return (0);
}
