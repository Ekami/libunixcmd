/*
** my_strncpy.c for my_strncpy.c in /home/godard_b//afs/rendu/piscine/Jour_06
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Mon Oct  8 10:18:28 2012 tuatini godard
** Last update Mon Oct  8 11:22:26 2012 tuatini godard
*/

char	*my_strncpy(char *dest, char *src, int n)
{
  int	i;
  int	char_nb;

  i = 0;
  char_nb = 0;
  while (src[char_nb] != 0)
    char_nb = char_nb + 1;
  while (i < n && src[i] != 0)
    {
      dest[i] = src[i];
      i = i + 1;
    }
  if (n > char_nb)
    dest[i] = '\0';
  return (dest);
}
