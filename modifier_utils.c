/*
** modifier_utils.c for modifier_utils in /home/godard_b/workspace/Projets/En_cours/printf/rendu/printf
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Tue Nov 13 02:36:48 2012 tuatini godard
** Last update Sun Nov 18 19:18:27 2012 tuatini godard
*/

int	get_lenghtmodifier_size(const char *modif_start)
{
  if (modif_start[1] == 'h' && modif_start[2] == 'h')
    return (3);
  else if (modif_start[1] == 'l' && modif_start[2] == 'l')
    return (3);
  else if (modif_start[1] == 'h')
    return (2);
  else if (modif_start[1] == 'l')
    return (2);
  else if (modif_start[1] == 'L')
    return (2);
  else if (modif_start[1] == 'q')
    return (2);
  else if (modif_start[1] == 'j')
    return (2);
  else if (modif_start[1] == 'z')
    return (2);
  else if (modif_start[1] == 't')
    return (2);
  else if (modif_start[1] == 'b')
    return (2);
  return (1);
}

int	get_conversionspecifier_size(const char *modif_start, int ret)
{
  if (modif_start[ret] == 'd' || modif_start[ret] == 'i')
    return (ret + 1);
  else if (modif_start[ret] == 'o' || modif_start[ret] == 'u')
    return (ret + 1);
  else if (modif_start[ret] == 'x' || modif_start[ret] == 'X')
    return (ret + 1);
  else if (modif_start[ret] == 'e' || modif_start[ret] == 'E')
    return (ret + 1);
  else if (modif_start[ret] == 'f' || modif_start[ret] == 'F')
    return (ret + 1);
  else if (modif_start[ret] == 'g' || modif_start[ret] == 'G')
    return (ret + 1);
  else if (modif_start[ret] == 'a' || modif_start[ret] == 'A')
    return (ret + 1);
  else if (modif_start[ret] == 'c' || modif_start[ret] == 's')
    return (ret + 1);
  else if (modif_start[ret] == 'C' || modif_start[ret] == 'S')
    return (ret + 1);
  else if (modif_start[ret] == 'p' || modif_start[ret] == 'n')
    return (ret + 1);
  else if (modif_start[ret] == 'm' || modif_start[ret] == '%')
    return (ret + 1);
  return (ret);
}
