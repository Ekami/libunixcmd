/*
** my_strcapitalize.c for my_strcapitalize in /home/godard_b//workspace/progs/Jour_06
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Tue Oct  9 15:09:05 2012 tuatini godard
** Last update Sun Nov 18 20:40:05 2012 tuatini godard
*/

char	*my_strcapitalize(char *str)
{
  int	i;
  int	bool;

  i = 0;
  my_strlowcase(str);
  str[0] = str[0] - 32;
  while (str[i] != 0)
    {
      if (str[i] >= 97 && str[i] <= 122 &&
	   str[i - 1] < 48 && str[i] > 57 && i != 0)
	{
	  str[i] = str[i] - 32;
	}
      i = i + 1;
    }
  return (str);
}
