/*
** my_strupcase.c for my_strupcase.c in /home/godard_b//workspace/progs/Jour_06
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Tue Oct  9 11:58:38 2012 tuatini godard
** Last update Tue Oct  9 12:05:40 2012 tuatini godard
*/

char	*my_strupcase(char *str)
{
  int	i;

  i = 0;
  while (str[i] != 0)
    {
      if (str[i] >= 97 && str[i] <= 122)
	str[i] = str[i] - 32;
      i = i + 1;
    }
  return (str);
}
