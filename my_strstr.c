/*
** my_strstr.c for my_strstr.c in /home/godard_b//workspace/progs/Jour_06
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Mon Oct  8 14:02:01 2012 tuatini godard
** Last update Mon Oct  8 15:56:19 2012 tuatini godard
*/

char	*str_match(char *str, char *to_find, int str_nb, int find_nb)
{
  int	i;
  int	j;
  int	str_match;

  i = 0;
  j = 0;
  while (i < str_nb)
    {
      str_match = 1;
      if (str[i] == to_find[0])
	{
	  while (j < find_nb)
	    {
	      if (!(str[i + j] == to_find[j]))
		str_match = 0;
	      j = j + 1;
	    }
	  if (str_match)
	    return (str + i);
	}
      j = 0;
      i = i + 1;
    }
  return (0);
}

char	*my_strstr(char *str, char *to_find)
{
  int	str_count;
  int	to_find_count;

  str_count = my_strlen(str);
  to_find_count = my_strlen(to_find);
  return (str_match(str, to_find, str_count, to_find_count));
}
