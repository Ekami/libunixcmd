/*
** printf_utils.c for printf_utils in /home/godard_b/workspace/Projets/En_cours/printf/rendu
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Mon Nov 12 13:38:57 2012 tuatini godard
** Last update Sun Nov 18 20:17:52 2012 tuatini godard
*/

#include <stdlib.h>

int	get_lenghtmodifier_size(const char *modif_start);
int	get_conversionspecifier_size(const char *modif_start, int ret);

int	find_modifier_offset(const char *modif_start)
{
  int	ret;
  int	i;

  i = 0;
  ret = get_lenghtmodifier_size(modif_start);
  ret = get_conversionspecifier_size(modif_start, ret);
  if (ret == 1)
    return (0);
   return (ret);
}

int	store_str(char **dest, int dest_i, const char *str, int size)
{
  int	i;
  char *tmp;

  i = 0;
  tmp = malloc(sizeof(char) * my_strlen(str) + 1);
  if (tmp == NULL)
    return (0);
  while (i < size)
    {
      tmp[i] = str[i];
      i = i + 1;
    }
  tmp[i] = 0;
  dest[dest_i] = tmp;
  return (1);
}

int	split_str_work(const char *str, int *ret_ind, char **ret)
{
  int	modif_lenght;
  int	j;
  int	i;

  j = 0;
  i = 0;
  while (i++ < my_strlen(str))
    if (str[i] == '%')
      {
	modif_lenght = find_modifier_offset(&(str[i]));
	if (modif_lenght)
	  {
	    store_str(ret, (*ret_ind)++, &(str[j]), i - j);
	    j = i;
	    store_str(ret, (*ret_ind)++, &(str[j]), modif_lenght);
	    j = i + modif_lenght;
	  }
	i = i + modif_lenght;
      }
  return (j);
}

void	split_str(const char *str, char **buffer)
{
  int	j;
  int	ret_ind;
  int	*modifiers_offset;

  ret_ind = 0;
  j = split_str_work(str, &ret_ind, buffer);
  if (str[j] == '%')
    {
      store_str(buffer, ret_ind++, &(str[j]), find_modifier_offset(&(str[j])));
      j = j + find_modifier_offset(&(str[j]));
      store_str(buffer, ret_ind++, &(str[j]), my_strlen(&(str[j])));
    }
  else
    store_str(buffer, ret_ind++, &(str[j]), my_strlen(&(str[j])));
  buffer[ret_ind] = 0;
}

void	free_all(char **str)
{
  int	i;

  i = 0;
  while (str[i])
    {
      free(str[i]);
      i = i + 1;
    }
  free(str);
}
