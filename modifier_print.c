/*
** modifier_print.c for modifier_print in /home/e-kami/workspace/epitech/Projets/En_cours/printf/my_printf/rendu/printf
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Sat Nov 17 04:55:45 2012 tuatini godard
** Last update Sun Nov 18 18:22:10 2012 tuatini godard
*/

#include <stdlib.h>
#include <stdarg.h>
#include <errno.h>
#include <stdio.h>
#include "my_printf.h"

int	print_signed_decimal(char *modifier, va_list ap)
{
  int	lenght;

  lenght = my_strlen(modifier);
  if (modifier[lenght - 1] == 'd' || modifier[lenght - 1] == 'i')
    {
      if (modifier[lenght - 2] == 'h' && modifier[lenght - 3] == 'h')
	my_put_nbr(get_only_given_bits(va_arg(ap, int), sizeof(char)));
      else if (modifier[lenght - 2] == 'h')
	my_put_nbr(get_only_given_bits(va_arg(ap, int), sizeof(short)));
      else if (modifier[lenght - 2] == 'l' && modifier[lenght - 3] == 'l')
	my_put_long_nbr(va_arg(ap, long long));
      else if (modifier[lenght - 2] == 'l')
	my_put_long_nbr(va_arg(ap, long));
      else
	my_put_nbr(va_arg(ap, int));
      return (1);
    }
  return (0);
}

int	print_char(char *modifier, va_list ap)
{
  int	lenght;

  lenght = my_strlen(modifier);
  if (modifier[lenght - 1] == 'c')
    {
      my_putchar(va_arg(ap, unsigned int));
      return (1);
    }
  return (0);
}

int	print_string(char *modifier, va_list ap)
{
  int	lenght;

  lenght = my_strlen(modifier);
  if (modifier[lenght - 1] == 's')
    {
      my_putstr(va_arg(ap, char *));
      return (1);
    }
  else if (modifier[lenght - 1] == 'S')
    {
      my_putstr_oct(va_arg(ap, unsigned char *));
      return (1);
    }
  return (0);
}

int	print_miscs(char *modifier, va_list ap)
{
  int	lenght;

  lenght = my_strlen(modifier);
  if (modifier[lenght - 1] == 'p')
    {
      print_ptr_in_hex(va_arg(ap, void *));
      return (1);
    }
  else if (modifier[lenght - 1] == 'm')
    {
      my_putstr(sys_errlist[errno]);
      return (1);
    }
  else if (modifier[lenght - 1] == '%')
    {
      my_putchar('%');
      return (1);
    }
  return (0);
}
