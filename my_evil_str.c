/*
** my_evil_str.c for my_evil_str.c in /home/godard_b/test/Jour_04
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Thu Oct  4 20:35:50 2012 tuatini godard
** Last update Thu Oct  4 22:18:45 2012 tuatini godard
*/

void	my_swap_char(char *c, char *b)
{
  char	temp;

  temp = *c;
  *c = *b;
  *b = temp;
}

char	*my_evil_str(char *str)
{
  int	count;
  int	i;
  int	j;
  int	mid;

  count = my_strlen(str);
  mid = count / 2;
  i = 0;
  j = count - 1;

  while (mid != 0)
    {
      my_swap_char(str + i, str + j);
      i = i + 1;
      j = j - 1;
      mid = mid - 1;
    }

  return (str);
}
