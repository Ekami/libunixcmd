/*
** my_strcpy.c for my_strcpy.c in /home/godard_b//afs/rendu/piscine/Jour_06
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Mon Oct  8 10:18:28 2012 tuatini godard
** Last update Mon Oct  8 11:00:29 2012 tuatini godard
*/

char	*my_strcpy(char *dest, char *src)
{
  int	i;

  i = 0;
  while (src[i] != 0)
    {
      dest[i] = src[i];
      i = i + 1;
    }
  dest[i] = '\0';
  return (dest);
}
