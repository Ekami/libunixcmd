##
## Makefile for Makefile in /home/godard_b//workspace/epitech/libmy
## 
## Made by tuatini godard
## Login   <godard_b@epitech.net>
## 
## Started on  Mon Oct 22 10:28:52 2012 tuatini godard
## Last update Sun Nov 18 18:15:57 2012 tuatini godard
##

SRC=	my_evil_str.c \
	my_find_prime_sup.c \
	my_getnbr.c \
	my_isneg.c \
	my_is_prime.c \
	my_power_rec.c \
	my_putchar.c \
	my_put_nbr.c \
	my_putstr.c \
	my_revstr.c \
	my_showmem.c \
	my_showstr.c \
	my_sort_int_tab.c \
	my_square_root.c \
	my_strcapitalize.c \
	my_strcat.c \
	my_strcmp.c \
	my_strcpy.c \
	my_strdup.c \
	my_str_isalpha.c \
	my_str_islower.c \
	my_str_isnum.c \
	my_str_isprintable.c \
	my_str_isupper.c \
	my_strlcat.c \
	my_strlen.c \
	my_strlowcase.c \
	my_strncat.c \
	my_strncmp.c \
	my_strncpy.c \
	my_strstr.c \
	my_strupcase.c \
	my_swap.c \
	my_printf.c \
	modifier_print.c \
	modifier_utils.c \
	printf_utils.c \
	base_converters.c \
	bits_manager.c \
	my_put_nbr2.c \
	print_unsigned_decimal.c \
	print_byte_in_hex.c

OBJS=	$(SRC:.c=.o)

NAME=	libmy.a

CC=	cc

all:	$(NAME)

$(NAME): $(OBJS)
	ar rc $(NAME) $(OBJS)
	ranlib $(NAME)
	chmod 640 libmy.a

clean:
	rm -f $(OBJS)

fclean:	clean
	rm -f $(NAME)

re:	fclean all
