/*
** my_sort_int_tab.c for my_sort_int_tab.c in /home/godard_b//test/Jour_04
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Fri Oct  5 12:15:34 2012 tuatini godard
** Last update Fri Oct  5 14:07:44 2012 tuatini godard
*/

int	my_sort_int_tab(int *tab, int size)
{
  int	tab_ret[size];
  int	i;
  int	j;
  int	k;

  i = 0;
  j = 0;
  k = 0;
  while (i < size)
    {
      while (j < size)
	{
	  if (tab[i] < tab[j])
	    {
	      k = tab[i];
	      tab[i] = tab[j];
	      tab[j] = k;
	    }
	  j = j + 1;
	}
      j = 0;
      i = i + 1;
    }
}
