/*
** my_printf.c for my_printf in /home/godard_b/workspace/Projets/En_cours/printf/rendu
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Sun Nov 11 22:36:29 2012 tuatini godard
** Last update Sun Nov 18 20:21:49 2012 tuatini godard
*/

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdarg.h>
#include "my_printf.h"

int	transform_modifier(char *modifier, va_list ap)
{
  if (print_char(modifier, ap))
    return (1);
  if (print_signed_decimal(modifier, ap))
    return (1);
  if (print_unsigned_decimal(modifier, ap))
    return (1);
  if (print_string(modifier, ap))
    return (1);
  if (print_miscs(modifier, ap))
    return (1);
  my_putstr(modifier);
  return (0);
}

int	my_printf(const char *format, ...)
{
  int		i;
  char		**str_splitted;
  va_list	ap;

  i = 0;
  va_start(ap, *format);
  str_splitted = malloc(sizeof(char *) * my_strlen(format));
  if (str_splitted == NULL)
    return (-1);
  split_str(format, str_splitted);
  while (str_splitted[i])
    {
      transform_modifier(str_splitted[i], ap);
      i = i + 1;
    }
  free_all(str_splitted);
  return (0);
}
