/*
** my_getnbr.c for my_getnbr in /home/godard_b//workspace/progs
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Thu Oct 11 14:55:53 2012 tuatini godard
** Last update Thu Oct 11 16:55:51 2012 tuatini godard
*/

int	power(int power)
{
  int	i;
  int	result;

  i = 0;
  result = 1;
  while (i < power)
    {
      result = result * 10;
      i = i + 1;
    }
  return (result);
}

int	to_int(char *str, int multiples_of_ten, int isneg)
{
  int	i;
  int	j;
  long	lastnum;

  j = 0;
  i = multiples_of_ten;
  lastnum = 0;
  while (i > 0)
    {
      lastnum = lastnum + (str[j] - 48) * power(i - 1);
      i = i - 1;
      j = j + 1;
    }
  if (isneg)
    lastnum = -lastnum;
  if (lastnum > 2147483647 || lastnum < -2147483647)
    return (0);
  return (lastnum);
}

int	my_getnbr(char *str)
{
  int	i;
  int	j;
  int	nb_neg;
  char	number[10];

  i = 0;
  j = 0;
  nb_neg = 0;
  while (str[i] == '-' || str[i] == '+')
    {
      if (str[i] == '-')
	nb_neg = nb_neg + 1;
      i = i + 1;
    }
  while (str[i] >= '0' && str[i] <= '9')
    {
      if (j >= 9)
	return (0);

      number[j] = str[i];
      j = j + 1;
      i = i + 1;
    }
  return (to_int(number, j, nb_neg % 2 == 0 ? 0 : 1));
}
