/*
** my_showmem.c for my_showmem in /home/e-kami/workspace/epitech/Jour_06
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Fri Oct 19 17:31:42 2012 tuatini godard
** Last update Sat Oct 20 12:07:41 2012 tuatini godard
*/

void	print_offset(int offset)
{
  int	i;
  int	nb_zero;

  i = 1;
  nb_zero = 8;
  while (offset / my_power_rec(10, i) != 0)
    i = i + 1;
  nb_zero = nb_zero - i;
  i = 0;
  while (i < nb_zero)
    {
      my_putchar('0');
      i = i + 1;
    }
  my_put_nbr(offset);
}

void	print_hex_of_str(char *str, int *fromOffset, int number_of_blocks)
{
  int	i;
  int	ibis;
  int	end_reached;

  i = 0;
  end_reached = 0;
  while (i < number_of_blocks)
    {
      ibis = *fromOffset + 2;
      while (*fromOffset < ibis)
	{
	  if (str[*fromOffset] == 0)
	    end_reached = 1;
	  if (!end_reached)
	    print_byte_in_hex(str[*fromOffset]);
	  else
	    my_putstr("  ");
	  *fromOffset = *fromOffset + 1;
	}
      my_putchar(' ');
      i = i + 1;
    }
}

void	print_str(char *str, int str_size)
{
  int	i;

  i = 0;
  while (i < str_size)
    {
      if (str[i] != 0)
	my_putchar(str[i]);
      else
	i = str_size;
      i = i + 1;
    }
}

int	my_showmem(char *str, int size)
{
  int	i;
  int	ibis;
  int	number_of_bytes_to_show;
  int	*i_offset;

  i = 0;
  i_offset = &i;
  number_of_bytes_to_show = 16;
  while (i < my_strlen(str))
    {
      print_offset(i);
      my_putchar(':');
      my_putchar(' ');
      ibis = i;
      print_hex_of_str(str, i_offset, number_of_bytes_to_show / 2);
      print_str(&str[ibis], number_of_bytes_to_show);
      my_putchar('\n');
    }
  return (0);
}
