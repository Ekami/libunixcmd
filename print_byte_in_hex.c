/*
** print_byte_in_hex.c for print_byte_int_hex in /home/e-kami/workspace/epitech/libmy
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Fri Oct 19 17:14:38 2012 tuatini godard
** Last update Sun Oct 21 17:51:14 2012 tuatini godard
*/

void	print_hex(int nb)
{
  int	temp;

  temp = 0;
  temp = nb / 16;
  if (temp <= 9)
    my_putchar(temp + 48);
  else
    my_putchar(temp + 87);
  temp = nb % 16;
  if (temp <= 9)
    my_putchar(temp + 48);
  else
    my_putchar(temp + 87);
}

void	print_byte_in_hex(int nb)
{
  if (nb > 255)
    return ;
  if (nb <= 9)
    {
      my_putchar('0');
      my_putchar(nb + 48);
    }
  else
    {
      if (nb >= 16)
	print_hex(nb);
      else
	{
	  my_putchar('0');
	  my_putchar(nb + 87);
	}
    }
}
